import UIKit
import CoreData
import Firebase

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var loadingSpinner: UIActivityIndicatorView!
    
    var transactionWithSections:[Int: [Transac]] = [0: [], 1: [], 2:[]]
    let sections = ["New", "Last week", "Older"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self

        loadingSpinner.transform = CGAffineTransform(scaleX: 3, y: 3)
        loadingSpinner.isHidden = false
        
        navigationController?.navigationBar.prefersLargeTitles = true
        
        print("start synchronisation")
        TransactionRepository.shared.synchronizeWithCloud(moc: getContext())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("register notification callback")
        NotificationCenter.default.addObserver(self, selector: #selector(endSynchronisation(_:)), name: NSNotification.Name(rawValue: "endSynchronisation"), object: nil)
        update()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "endSynchronisation"), object: nil)
    }
    
    fileprivate func update() {
        balanceLabel.text = "\(TransactionRepository.shared.getBalance(context: self.getContext()).roundForMoney()) €"
        let transactions = TransactionRepository.shared.getAll(context: self.getContext())
        transactionWithSections = [0: [],1: [],2:[]]
        let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: Date())!
        let lastWeek = Calendar.current.date(byAdding: .day, value: -7, to: Date())!
        for t in transactions {
            if t.date > yesterday {
                transactionWithSections[0]!.append(t)
            }else if t.date > lastWeek{
                transactionWithSections[1]!.append(t)
            }else{
                transactionWithSections[2]!.append(t)
            }
        }
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactionWithSections[section]!.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section]
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let transaction = transactionWithSections[indexPath.section]![indexPath.item]
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionCell", for: indexPath) as! TableViewCell
        cell.amountLabel.textColor = UIColor.black
        if (transaction.amount > 0){
            cell.amountLabel.text = "\(transaction.amount) €"
            cell.amountLabel.textColor = UIColor(red:0.27, green:0.67, blue:0.00, alpha:1.0)
        }
        cell.amountLabel.text = "\(transaction.amount) €"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        let selectedDate = dateFormatter.string(from: transaction.date)
        cell.dateLabel.text = selectedDate
        cell.descriptionLabel.text = transaction.description
        cell.categoryImage.image = UIImage(named: transaction.category.lowercased())!
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let transacId = transactionWithSections[indexPath.section]![indexPath.item].id
            let transacFirebaseId = transactionWithSections[indexPath.section]![indexPath.item].firebaseId
            TransactionRepository.shared.removeTransaction(id: transacId!, firebaseId: transacFirebaseId!, context: self.getContext())
            update()
        }
    }
    
    @IBAction func logOutPressed(_ sender: Any) {
        do {
            try Auth.auth().signOut()
            TransactionRepository.shared.erase(context: self.getContext())
            
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "loginNavigation")
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
            
            print("log out sucess")
        } catch {
            print("error: there was a problem logging out")
        }
    }
    
    @objc func endSynchronisation(_ notification:Notification) {
        print("end synchronisation + update list")
        update()
        loadingSpinner.isHidden = true
    }
}
