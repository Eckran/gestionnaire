import Foundation
import CoreData

class Transac:Equatable {
    static func == (lhs: Transac, rhs: Transac) -> Bool {
        return lhs.amount == rhs.amount &&
        lhs.description == rhs.description &&
        lhs.category == rhs.category &&
        lhs.date == rhs.date
    }
    
    var amount : Double
    var date : Date
    var description : String
    var category : String
    var id : NSManagedObjectID?
    var firebaseId : String?
    
    init(amount : Double, date : Date, description : String, category : String, id : NSManagedObjectID? = nil, firebaseId : String? = nil) {
        self.amount = amount
        self.date = date
        self.description = description
        self.category = category
        self.id = id
        self.firebaseId = firebaseId
    }
    
    public var toString: String { return "amount:\(amount) date:\(date) description:\(description) category:\(category)" }
    
}
