import Foundation
import CoreData
import Firebase

class TransactionRepository {
    
    static let shared = TransactionRepository()
    var ref: DatabaseReference!
    
    private init(){
        ref = Database.database().reference()
    }
    
    fileprivate func addToCloud(_ newTransaction: Transaction) {
        if let user = Auth.auth().currentUser {
            let value = newTransaction.toDictionnary();
            self.ref.child("users").child(user.uid).child("transactions").child(newTransaction.firebaseId!).setValue(value)
        }
    }
    
    func eraseCloud(firebaseId : String) {
        if let user = Auth.auth().currentUser {
            self.ref.child("users").child(user.uid).child("transactions").child(firebaseId).removeValue()
        }
    }

    func synchronizeWithCloud(moc: NSManagedObjectContext) {
        if let user = Auth.auth().currentUser {
            let transactionsInDatabase = getAll(context: moc)
            self.ref.child("users").child(user.uid).observeSingleEvent(of: .value, with: { (data) in
                for child in data.children 	{
                    let snap = child as! DataSnapshot
                    let dict = snap.value as! [String: [String:Any?]]
                    for key in dict {
                        let currentValue = Transac(amount:key.value["amount"] as! Double, date: Date(timeIntervalSinceReferenceDate: TimeInterval(key.value["date"] as! TimeInterval)), description: key.value["desc"] as! String, category: key.value["cate"] as! String)
                        if !transactionsInDatabase.contains(currentValue){
                            print(currentValue.toString)
                            self.AddTransaction(transac: currentValue, context: moc, false)
                        }
                    }
                }
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endSynchronisation"), object: nil)
            })
        }
    }
    
    func AddTransaction(transac : Transac, context : NSManagedObjectContext, _ synchronise: Bool = true) {
        let uuid = NSUUID().uuidString
        
        let transaction = NSEntityDescription.entity(forEntityName: "Transaction", in: context)
        let newTransaction = NSManagedObject(entity: transaction!, insertInto: context)
        newTransaction.setValue(transac.amount, forKey: "amount")
        newTransaction.setValue(transac.date, forKey: "date")
        newTransaction.setValue(transac.description, forKey: "desc")
        newTransaction.setValue(transac.category, forKey: "cate")
        newTransaction.setValue(uuid, forKey: "firebaseId")
        
        if synchronise {addToCloud(newTransaction as! Transaction)}
        
        do {
            try context.save()
        } catch {
            print("error context save")
        }
    }
    
    func TransactionSize(context : NSManagedObjectContext) -> Int{
        return getAll(context: context).count
    }
    
    func getBalance(context : NSManagedObjectContext) -> Double{
       return getAll(context: context).reduce(0, {x, y in
            x + y.amount
        })
    }
    
    func getAll(context : NSManagedObjectContext) -> [Transac]{
        var listTransac : [Transac] = []
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Transaction")
        request.returnsObjectsAsFaults = false
        let sort = NSSortDescriptor(key: "date", ascending: false)
        request.sortDescriptors = [sort]
        do {
            let result = try context.fetch(request)
            for data in result as! [Transaction] {
                let transac = Transac(amount: data.amount, date: data.date!, description: data.desc!, category: data.cate!, id : data.objectID, firebaseId: data.firebaseId!)
                listTransac.append(transac)
            }
            return listTransac
        } catch {
            
            print("Failed")
        }
        return listTransac
    }
    
    func removeTransaction(id : NSManagedObjectID, firebaseId : String, context : NSManagedObjectContext){
        let fetchedRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Transaction")
        fetchedRequest.predicate = NSPredicate(format: "self == %@", id)
        do {
            let list = try context.fetch(fetchedRequest)
            let transactionToDelete = list[0] as! NSManagedObject
            context.delete(transactionToDelete)
            
            do{
                try context.save()
                eraseCloud(firebaseId: firebaseId)
            } catch {
                print(error)
            }
        } catch {
            print(error)
        }
        
    }
    
    func getAllFromMonth(context : NSManagedObjectContext) -> [Transac]{
        var listTransac : [Transac] = []
        let fetchedRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Transaction")
        let minDate =  Calendar.current.date(byAdding: .day, value: -30, to: Date())
        fetchedRequest.predicate = NSPredicate(format: "date >= %@ AND amount < 0", minDate! as NSDate)
        do {
            let result = try context.fetch(fetchedRequest)
            for data in result as! [Transaction] {
                let transac = Transac(amount: data.amount, date: data.date!, description: data.desc!, category: data.cate!, id : data.objectID, firebaseId: data.firebaseId!)
                listTransac.append(transac)
            }
            return listTransac
        } catch {
            
            print("Failed")
        }
        return listTransac
    }
    
    func erase(context : NSManagedObjectContext)
    {
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Transaction")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do
        {
            try context.execute(deleteRequest)
            try context.save()
        }
        catch
        {
            print ("There was an error")
        }
    }
    
}

extension Transaction {
    func toDictionnary() -> [String: Any]{
        return [
            "amount" : self.amount,
            "date": self.date?.timeIntervalSinceReferenceDate,
            "desc": self.desc,
            "cate": self.cate
        ]
    }
}
