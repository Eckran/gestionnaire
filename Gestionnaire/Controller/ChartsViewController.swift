import UIKit
import Firebase
import Charts

class ChartsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ChartViewDelegate {
    
    @IBOutlet weak var pieChart: PieChartView!
    @IBOutlet weak var chartsTableView: UITableView!
    
    var mappedValues : [String: [Transac]] = [:]
    var finalCategory : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.chartsTableView.delegate = self
        self.chartsTableView.dataSource = self
        pieChart.delegate = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        pieChartUpdate()
        
    }
    
    func pieChartUpdate () {
        let transactions = TransactionRepository.shared.getAllFromMonth(context: getContext())
        var expenseByCategory: [String: Double] = [:]
        mappedValues = [:]
        var totalAmount = 0.0
        for transaction in transactions {
            if expenseByCategory[transaction.category] != nil {
                expenseByCategory[transaction.category] = expenseByCategory[transaction.category]! + transaction.amount
                totalAmount+=fabs(transaction.amount)
                
                mappedValues[transaction.category]?.append(transaction)
            }else{
                expenseByCategory[transaction.category] = transaction.amount
                totalAmount+=fabs(transaction.amount)
                
                mappedValues[transaction.category] = [transaction]
            }
        }
        var dataSet:[PieChartDataEntry] = []
        for (k,v) in expenseByCategory {
            dataSet.append(PieChartDataEntry(value: fabs(v)/totalAmount*100.0, label: k))
        }

        let pieChartDataSet = PieChartDataSet(values: dataSet, label: "Category")
        pieChartDataSet.colors = ChartColorTemplates.joyful()
        let data = PieChartData(dataSet: pieChartDataSet)
        pieChart.data = data
        pieChart.chartDescription?.text = "Percentage of spending this month"
        updatePieChartTotalValue()
        self.chartsTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mappedValues[finalCategory]?.count ?? mappedValues.getByIndex(i: section).value.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return mappedValues[finalCategory] != nil ? 1 : mappedValues.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return finalCategory == "" ? mappedValues.getByIndex(i: section).key : finalCategory
    }
    
    fileprivate func formatCell(_ transaction:Transac, _ chartsCell: ChartsTableViewCell) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        let selectedDate = dateFormatter.string(from: transaction.date)
        
        chartsCell.titleLabel.text = transaction.description
        chartsCell.dateLabel.text = selectedDate
        chartsCell.amountLabel.text = String(transaction.amount)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let chartsCell = tableView.dequeueReusableCell(withIdentifier: "chartsDetailCell", for: indexPath) as! ChartsTableViewCell
        
        if let t = mappedValues[finalCategory]{
            formatCell(t[indexPath.item], chartsCell)
        }else{
            formatCell(mappedValues.getByIndex(i: indexPath.section).value[indexPath.item], chartsCell)
        }
        
        return chartsCell
    }
    
    public func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight)
    {
        if let category = (entry as! PieChartDataEntry).label {
            finalCategory = category
        }
        updatePieChartTotalValue()
        self.chartsTableView.reloadData()
    }
    
    fileprivate func updatePieChartTotalValue() {
        var centerValue=0.0
        if let _ = mappedValues[finalCategory] {
            centerValue = mappedValues[finalCategory]?.reduce(0) {$0 + $1.amount} ?? 0
        }else{
            for (_,v) in mappedValues {
                centerValue+=v.reduce(0) {$0 + $1.amount}
            }
        }
        pieChart.centerText = "\(centerValue) €"
        pieChart.notifyDataSetChanged()
    }
    
    public func chartValueNothingSelected(_ chartView: ChartViewBase)
    {
        finalCategory = ""
        updatePieChartTotalValue()
        self.chartsTableView.reloadData()
    }

}

extension Dictionary {
    func getByIndex(i: Int) -> (key: Key, value: Value) {
        return self[index(startIndex, offsetBy: i)]
    }
}
