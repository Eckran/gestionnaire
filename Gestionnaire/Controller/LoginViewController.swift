import UIKit
import Firebase

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.emailTextField.delegate = self
        self.passwordTextField.delegate = self
        self.hideKeyboardWhenTappedAround()
        TransactionRepository.shared.erase(context: getContext())
        if let _ = Auth.auth().currentUser {
            goToHome()
        }
    }
    
    fileprivate func generateRandomData(){
        for _ in 1...30 {
            let amount = Double.random(in: -100.0 ..< 100.0).roundForMoney()
            let category = AddTransactionViewController.categoryTab[Int.random(in: 0...4)]
            TransactionRepository.shared.AddTransaction(transac: Transac(amount: amount, date: Calendar.current.date(byAdding: .day, value: Int.random(in: -50..<1), to: Date())!, description: "aze\(Int.random(in: 1..<6))", category: category), context: getContext())
        }
    }
    
    @IBAction func LoginPressed(_ sender: UIView) {
        Auth.auth().signIn(withEmail: emailTextField.text!, password: passwordTextField.text!) { (user, error) in
            if let error = error {
                print(error)
                let alert = UIAlertController(title: "Erreur:", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
                self.present(alert, animated: true)
                UIView.animate(withDuration: 0.1, animations: {
                    sender.frame.origin.x += 10
                }){ _ in
                    UIView.animate(withDuration: 0.1, delay: 0, options: [.autoreverse], animations: {
                        sender.frame.origin.x -= 10
                    })
                }
            } else {
                self.goToHome()
            }
        }
    }
    
    fileprivate func goToHome(){
        print("login sucess")
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "tabBarController") 
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = redViewController
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension Double {
    
    func roundForMoney()->Double {
        return Double((self*100).rounded()/100)
    }
}
