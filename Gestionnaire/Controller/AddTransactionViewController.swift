
import UIKit
import CoreData

class AddTransactionViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var booleanChoice: UISegmentedControl!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var categoryPicker: UIPickerView!
    @IBOutlet var toolBar: UIToolbar!
    
    
    @IBOutlet weak var scrollview: UIScrollView!
    
    public static let categoryTab = ["Food", "House", "Hobby", "Healing", "Transport"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.amountTextField.delegate = self
        amountTextField.inputAccessoryView = self.toolBar
        self.descriptionTextField.delegate = self
        self.hideKeyboardWhenTappedAround()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
        
        categoryPicker.delegate = self
        categoryPicker.dataSource = self
    }
    
    @IBAction func didTapDoneButton(_ sender: UIBarButtonItem) {
        view.endEditing(true)
    }
    
    @IBAction func SaveButtonPressed(_ sender: Any) {
        datePicker.datePickerMode = UIDatePicker.Mode.date
        
        let selectedSegment = booleanChoice.selectedSegmentIndex
        
        let number = NumberFormatter().number(from: amountTextField.text!)
        if let number = number {
            var doubleValue = Double(truncating: number)
            if(selectedSegment != 0) {
                doubleValue = -doubleValue
            }
            let selectedCategory = AddTransactionViewController.categoryTab[categoryPicker.selectedRow(inComponent: 0)]
            let transaction = Transac(amount: doubleValue, date: datePicker.date, description: descriptionTextField.text!, category: selectedCategory)
            // UUID, qu'on ajoute en base
            TransactionRepository.shared.AddTransaction(transac: transaction, context : self.getContext())
            dismiss(animated: true, completion: nil)
        } else {
            print("error add transaction: number")
        }
    }

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc func keyboardWillShow(notification:NSNotification){
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = scrollview.contentInset
        contentInset.bottom = keyboardFrame.size.height
        scrollview.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollview.contentInset = contentInset
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return AddTransactionViewController.categoryTab.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return AddTransactionViewController.categoryTab[row]
    }
    
    @IBAction func CancelButtonPressed(_ sender: Any) {
        let alert = UIAlertController(title: "Cancel?", message: "are you sure to cancel? you will lose all entered field", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: {action in
            self.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
}

extension UIViewController {
    func getContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
}
