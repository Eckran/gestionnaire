import UIKit
import Firebase

class RegisterViewController: UIViewController, UITextFieldDelegate {

    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var verifyTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.emailTextField.delegate = self
        self.passwordTextField.delegate = self
        self.verifyTextField.delegate = self
        self.hideKeyboardWhenTappedAround()
    }
    
    @IBAction func signinPressed(_ sender: Any) {
        if (passwordTextField.text == verifyTextField.text){
            Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { (user, error) in
                if let error = error {
                    print(error)
                    let alert = UIAlertController(title: "Erreur:", message: error.localizedDescription, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
                    self.present(alert, animated: true)
                } else {
                    print("Register sucess")
                    
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
